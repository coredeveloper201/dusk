<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ClassicHomeTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://www.classichome.com/customer/account/create/')
                ->assertSee('HAVE AN ACCOUNT?')
                ->type('#email', 'vendors@plaidfox.com')
                ->type('#pass', 'Success7')
                ->click('#send2')
                ->assertUrlIs('https://www.classichome.com/customer/account/');

        });
    }
}
