<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
class FourHandTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://marketplace.fourhands.com/login')
                ->assertSee('Your Style Connection')
                ->type('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_UserName', '75839')
                ->type('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_Password', 'Success7')
                ->click('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_btnLogon')
                ->assertUrlIs('https://marketplace.fourhands.com/home');

        });
    }
}
